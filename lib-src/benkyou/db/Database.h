#ifndef _BENKYOU_DB_DATABASE_H
#define _BENKYOU_DB_DATABASE_H

#ifndef _BENKYOU_MODEL_ID_H
#include <benkyou/model/Id.h>
#endif

#ifndef _BENKYOU_MODEL_STUDENT_H
#include <benkyou/model/Student.h>
#endif

#ifndef _BENKYOU_MODEL_ASPECT_H
#include <benkyou/model/Aspect.h>
#endif

#ifndef _BENKYOU_MODEL_CONCEPT_H
#include <benkyou/model/Concept.h>
#endif

#ifndef _BENKYOU_MODEL_GOAL_H
#include <benkyou/model/Goal.h>
#endif

#include <memory>

namespace benkyou {
   namespace db {
      class Database
      {
            /**
             * Default constructor.
             */
         protected:
            Database();

            /**
             * Destructor.
             */
         protected:
            virtual ~Database() = 0;

            /**
             * Load a student by id.
             * @param id a student id
             * @return a pointer to a student
             */
         public:
            virtual ::std::shared_ptr< ::benkyou::model::Student> findStudent(
                  ::std::shared_ptr< const ::benkyou::model::Id> id) = 0;

            /**
             * Load a goal by id.
             * @param id a goal id
             * @return a pointer to a goal
             */
         public:
            virtual ::std::shared_ptr< ::benkyou::model::Goal> findGoal(
                  ::std::shared_ptr< const ::benkyou::model::Id> id) = 0;

            /**
             * Load a concept by id.
             * @param id a concept id
             * @return a pointer to a concept
             */
         public:
            virtual ::std::shared_ptr< ::benkyou::model::Concept> findConcept(
                  ::std::shared_ptr< const ::benkyou::model::Id> id) = 0;

            /**
             * Load an aspect by id.
             * @param id an aspect id
             * @return a pointer to an aspect
             */
         public:
            virtual ::std::shared_ptr< ::benkyou::model::Aspect> findAspect(
                  ::std::shared_ptr< const ::benkyou::model::Id> id) = 0;

      };
   }
}
#endif
