#ifndef _BENKYOU_MODEL_PRACTICE_H
#define _BENKYOU_MODEL_PRACTICE_H

#ifndef _BENKYOU_MODEL_ACTIVITY_H
#include <benkyou/model/Activity.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {
      class Goal;

      /**
       * Practice is used to by students to practice any newly learned concepts
       * and aspects. Once a student has learned enough new material,
       * a quiz is generated.
       */
      class Practice : public Activity
      {
            Practice(const Practice&) = delete;
            Practice& operator=(const Practice&) = delete;

            /** Default constructor */
         protected:
            Practice();

            /** Destructor */
         public:
            virtual ~Practice() = 0;
      };
   }
}

#endif
