#ifndef _BENKYOU_MODEL_QUIZ_H
#define _BENKYOU_MODEL_QUIZ_H

#ifndef _BENKYOU_MODEL_ACTIVITY_H
#include <benkyou/model/Activity.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {

      /**
       * A quiz is used to determine the progress a student has made
       * towards a goal.
       */
      class Quiz : public Activity
      {
            Quiz(const Quiz&) = delete;
            Quiz& operator=(const Quiz&) = delete;
            /** Default constructor */
         protected:
            Quiz();

            /** Destructor */
         public:
            virtual ~Quiz() = 0;

      };
   }
}

#endif
