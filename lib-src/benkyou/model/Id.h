#ifndef _BENKYOU_MODEL_ID_H
#define _BENKYOU_MODEL_ID_H


namespace benkyou {
   namespace model {

      /**
       * This class captures the result of a single test question, or a set of questions.
       */
      class Id
      {
            Id(const Id&) = delete;
            Id& operator=(const Id&) = delete;

            /** Default constructor */
         protected:
            Id();

            /** Destructor */
         public:
            virtual ~Id() = 0;

            /**
             * Compare this id to another id.
             * @return -1 if this id is "before" the other id, 0 if they are exactly the same, or 1 if this id "after" the other id.
             */
         public:
            virtual int compare(const Id& other) const = 0;
      };
   }
}

#endif
