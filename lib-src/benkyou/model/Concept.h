#ifndef _BENKYOU_MODEL_CONCEPT_H
#define _BENKYOU_MODEL_CONCEPT_H

#ifndef _BENKYOU_MODEL_TARGET_H
#include <benkyou/model/Target.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {
      class Aspect;
      class Id;

      /**
       * A concept is a something that a user wants to learn and master.
       * A concept is learned through its aspects.
       */
      class Concept : public Target
      {
            Concept(const Concept&) = delete;
            Concept& operator=(const Concept&) = delete;
            /** Default constructor */
         protected:
            Concept();

            /** Destructor */
         public:
            virtual ~Concept() = 0;

            /**
             * The unique id for this object.
             * @return the id for this concept
             */
         public:
            virtual ::std::shared_ptr< const Id> id() const = 0;

            /**
             * Get the aspects of this concept that should be mastered. The aspects are also returned
             * via the target() method, but that method may include additional other concepts.
             * @return the aspects
             */
         public:
            virtual ::std::vector< ::std::shared_ptr< const Aspect>> aspects() const = 0;

      };
   }
}

#endif
