#ifndef _BENKYOU_MODEL_COLLECTOR_H
#define _BENKYOU_MODEL_COLLECTOR_H

#include <vector>

namespace benkyou {
   namespace model {

      /**
       * A collector is an object to which objects can be added, but which determines which
       * items are accepted.
       */
      template<class T> class Collector
      {
            Collector(const Collector&) = delete;
            Collector& operator=(const Collector&) = delete;
            /** Default constructor */
         protected:
            Collector();

            /** Destructor */
         public:
            virtual ~Collector() = 0;

            /**
             * Add an item to this collector.
             * @param item an item
             * @return true if the item was added, false otherwise
             */
         public:
            virtual bool add(const T& object) = 0;

            /**
             * Get the items that have been added to this collector.
             * @return the item that were added.
             */
         public:
            virtual ::std::vector< T> items() const = 0;
      };
   }
}

#endif
