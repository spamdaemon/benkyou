#ifndef _BENKYOU_MODEL_LESSON_H
#define _BENKYOU_MODEL_LESSON_H

#ifndef _BENKYOU_MODEL_ACTIVITY_H
#include <benkyou/model/Activity.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {

      /**
       * A lesson is used to introduce a student to new concepts. Upon
       * completion of a lesson, practice exercises will be used to
       */
      class Lesson : public Activity
      {
            Lesson(const Lesson&) = delete;
            Lesson& operator=(const Lesson&) = delete;

            /** Default constructor */
         protected:
            Lesson();

            /** Destructor */
         public:
            virtual ~Lesson() = 0;

      };
   }
}

#endif
