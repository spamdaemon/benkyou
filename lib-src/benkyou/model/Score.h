#ifndef _BENKYOU_MODEL_SCORE_H
#define _BENKYOU_MODEL_SCORE_H

namespace benkyou {
   namespace model {

      /**
       * A score is used as a measure of progress towards a learning target.
       */
      typedef double Score;

   }
}

#endif
