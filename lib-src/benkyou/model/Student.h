#ifndef _BENKYOU_MODEL_STUDENT_H
#define _BENKYOU_MODEL_STUDENT_H

#ifndef _BENKYOU_MODEL_COLLECTOR_H
#include <benkyou/model/Collector.h>
#endif

#include <memory>
#include <vector>

namespace benkyou {
   namespace model {
      class Progress;
      class Plan;
      class Id;

      /**
       * A student is someone with tailored lessons and general progress status.
       */
      class Student
      {
            Student(const Student&) = delete;
            Student& operator=(const Student&) = delete;

            /** Default constructor */
         protected:
            Student();

            /** Destructor */
         public:
            virtual ~Student() = 0;

            /**
             * The unique id for this student.
             * @return the id for this student
             */
         public:
            virtual ::std::shared_ptr< const Id> id() const = 0;

            /**
             * The lesson plans for the student.
             * @return a set of known lesson plans that the student has completed, is current working on, or are planned for the future.
             */
         public:
            virtual ::std::vector< ::std::shared_ptr< const Plan>> plans() const = 0;

            /**
             * Get the progress this student has made towards specified targets. It is
             * the collector that determines for which targets progress is returned.
             * @param collector a collector to which student progress will be added
             */
         public:
            virtual void collectProgress(Collector< ::std::shared_ptr< const Progress>>& collector) const = 0;

            /**
             * Save the specified progress information.
             * @param progress the new progress information.
             */
         public:
            virtual void saveProgress(::std::vector< ::std::shared_ptr< const Progress>>& progress) = 0;

      };
   }
}

#endif
