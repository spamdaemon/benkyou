#ifndef _BENKYOU_MODEL_ASPECT_H
#define _BENKYOU_MODEL_ASPECT_H

#ifndef _BENKYOU_MODEL_TARGET_H
#include <benkyou/model/Target.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {
      class Concept;
      class Id;

      /**
       * An aspect is a single piece of information that is usually attached
       * to a larger concept.
       */
      class Aspect : public Target
      {
            Aspect(const Aspect&) = delete;
            Aspect& operator=(const Aspect&) = delete;
            /** Default constructor */
         protected:
            Aspect();

            /** Destructor */
         public:
            virtual ~Aspect() = 0;

            /**
             * The unique id for this object.
             * @return the id for this aspect
             */
         public:
            virtual ::std::shared_ptr< const Id> id() const = 0;

            /**
             * Get the concept that defines this aspect
             * @return the larger concept
             */
         public:
            virtual ::std::shared_ptr< const Concept> concept() const = 0;
      };
   }
}

#endif
