#ifndef _BENKYOU_MODEL_API_H
#define _BENKYOU_MODEL_API_H

#ifndef _BENKYOU_MODEL_ID_H
#include <benkyou/model/Id.h>
#endif

#ifndef _BENKYOU_MODEL_GOAL_H
#include <benkyou/model/Goal.h>
#endif

#ifndef _BENKYOU_MODEL_CONCEPT_H
#include <benkyou/model/Concept.h>
#endif

#ifndef _BENKYOU_MODEL_ASPECT_H
#include <benkyou/model/Aspect.h>
#endif

#ifndef _BENKYOU_MODEL_STUDENT_H
#include <benkyou/model/Student.h>
#endif

#ifndef _BENKYOU_MODEL_TARGET_H
#include <benkyou/model/Target.h>
#endif

#ifndef _BENKYOU_MODEL_PROGRESS_H
#include <benkyou/model/Progress.h>
#endif

#ifndef _BENKYOU_MODEL_TESTSCORE_H
#include <benkyou/model/TestScore.h>
#endif

#ifndef _BENKYOU_MODEL_SCORE_H
#include <benkyou/model/Score.h>
#endif

#ifndef _BENKYOU_MODEL_COLLECTOR_H
#include <benkyou/model/Collector.h>
#endif

#ifndef _BENKYOU_MODEL_ACTIVITY_H
#include <benkyou/model/Activity.h>
#endif

#ifndef _BENKYOU_MODEL_LESSON_H
#include <benkyou/model/Lesson.h>
#endif

#ifndef _BENKYOU_MODEL_PRACITICE_H
#include <benkyou/model/Practice.h>
#endif

#ifndef _BENKYOU_MODEL_QUIZ_H
#include <benkyou/model/Quiz.h>
#endif



#endif
