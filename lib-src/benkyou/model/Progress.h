#ifndef _BENKYOU_MODEL_PROGRESS_H
#define _BENKYOU_MODEL_PROGRESS_H

#ifndef _BENKYOU_MODEL_SCORE_H
#include <benkyou/model/Score.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {
      class Target;

      /**
       * This class captures the students progress towards a reaching a goal, a understanding a concept, or
       * mastering a aspects.
       * The basic progress information is a score in the range 0 to 1, where 0 indicates that no progress has
       * been made, and 1 means that a student has complete mastered the task.
       */
      class Progress
      {
            Progress(const Progress&) = delete;
            Progress& operator=(const Progress&) = delete;

            /** Default constructor */
         protected:
            Progress();

            /** Destructor */
         public:
            virtual ~Progress() = 0;

            /**
             * Get the progress score.
             * @return the progress score
             */
         public:
            virtual Score score() const = 0;

            /**
             * Get the target for which this is a score.
             * @return the target
             */
         public:
            virtual ::std::shared_ptr< const Target> target() const = 0;
      };
   }
}

#endif
