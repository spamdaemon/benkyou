#ifndef _BENKYOU_MODEL_PLAN_H
#define _BENKYOU_MODEL_PLAN_H

#include <cstddef>
#include <memory>

namespace benkyou {
   namespace model {
      class Activity;
      class Goal;
      class Progress;
      class Student;

      /**
       *  A plan is a set sequence of activities that help a student achieve a learning goal.
       *  The number of lessons in a plan is typically fixed, but the total number of activities
       *  may depend on the student's progress.
       */
      class Plan
      {
            Plan(const Plan&) = delete;
            Plan& operator=(const Plan&) = delete;

            /** Default constructor */
         protected:
            Plan();

            /** Destructor */
         public:
            virtual ~Plan() = 0;

            /**
             * The goal that this lesson plan will help achieve.
             * @return the goal for this activity.
             */
         public:
            virtual ::std::shared_ptr< const Goal> goal() const = 0;

            /**
             * Get the student for whom this plan was designed.
             * @return the student
             */
         public:
            virtual ::std::shared_ptr< const Student> student() const = 0;

            /**
             * Create a new activity in this lesson plan.
             * @return a new activity based on the student progress or nullptr if there
             * are no activities to be performed at this time.
             */
         public:
            virtual ::std::unique_ptr<const Activity> nextActivity() = 0;

            /**
             * Get the number of lessons in this plan. If this plan does not contain
             * lessons, then this number is 0.
             * @return the total number of lessons in this plan.
             */
         public:
            virtual size_t lessonCount() const = 0;

            /**
             * Get the number of lessons remaining to be completed.
             * @return the number of lessons remaining in this plan.
             */
         public:
            virtual size_t lessonRemaining() const = 0;

            /**
             * Get the progress made so far towards the goal. The progress is normalized by
             * by the maximum possible progress that student should have made.
             * @return the progress made so far
             */
         public:
            virtual ::std::shared_ptr< Progress> progress() const = 0;

      };
   }
}

#endif
