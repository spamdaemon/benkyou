#ifndef _BENKYOU_MODEL_TARGET_H
#define _BENKYOU_MODEL_TARGET_H

#include <memory>
#include <vector>

namespace benkyou {
   namespace model {

      /**
       * The target class is short for Learning Target and the class defines
       * what is learning within the learning system.
       */
      class Target
      {
            Target(const Target&) = delete;
            Target& operator=(const Target&) = delete;
            /** Default constructor */
         protected:
            Target();

            /** Destructor */
         public:
            virtual ~Target() = 0;

            /**
             * Get the targets that mastering this goal requires.
             * @return a list of targets, which may be aspects or concepts, or other other goals
             */
         public:
            virtual ::std::vector< ::std::shared_ptr< const Target>> targets() const = 0;
      };
   }
}

#endif
