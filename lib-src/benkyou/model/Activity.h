#ifndef _BENKYOU_MODEL_ACTIVITY_H
#define _BENKYOU_MODEL_ACTIVITY_H

#include <memory>
#include <vector>

namespace benkyou {
   namespace model {
      class Aspect;
      class Plan;
      class Progress;

      /**
       *  This class defines a student specified learning activity, such as a lesson, a practice session,
       *  or a quiz. Each activity involves a set of learning targets.
       */
      class Activity
      {
            Activity(const Activity&) = delete;
            Activity& operator=(const Activity&) = delete;

            /** Default constructor */
         protected:
            Activity();

            /** Destructor */
         public:
            virtual ~Activity() = 0;

            /**
             * The lesson plan associated with this activity.
             * @return the plan
             */
         public:
            virtual ::std::shared_ptr< const Plan> plan() const = 0;

            /**
             * The aspects involved in this activity.
             * @return the information being taught, tested, etc.
             */
         public:
            virtual ::std::vector< ::std::shared_ptr< const Aspect>> aspects() const = 0;

            /**
             * Execute this activity which will result in progress information being updated.
             * @return progress information that was updated during his activity.
             */
         public:
            virtual ::std::vector< ::std::shared_ptr< Progress>> perform() const = 0;
      };
   }
}

#endif
