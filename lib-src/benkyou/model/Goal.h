#ifndef _BENKYOU_MODEL_GOAL_H
#define _BENKYOU_MODEL_GOAL_H

#ifndef _BENKYOU_MODEL_TARGET_H
#include <benkyou/model/Target.h>
#endif

#include <memory>
#include <vector>

namespace benkyou {
   namespace model {
      class Id;
      class Plan;
      class Student;

      /**
       * A goal defines concepts and aspects that a user wants to learn.
       */
      class Goal : public Target
      {
            Goal(const Goal&) = delete;
            Goal& operator=(const Goal&) = delete;
            /** Default constructor */
         protected:
            Goal();

            /** Destructor */
         public:
            virtual ~Goal() = 0;

            /**
             * The unique id for this object.
             * @return the id for this goal
             */
         public:
            virtual ::std::shared_ptr< const Id> id() const = 0;

            /**
             * Create a lesson plan for the specified student.
             * @param student a student
             * @return a lesson plan
             */
         public:
            virtual ::std::shared_ptr< Plan> getLessonPlan(::std::shared_ptr< const Student> student) = 0;
      };
   }
}

#endif
