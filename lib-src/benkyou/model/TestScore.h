#ifndef _BENKYOU_MODEL_TESTSCORE_H
#define _BENKYOU_MODEL_TESTSCORE_H

#ifndef _BENKYOU_MODEL_SCORE_H
#include <benkyou/model/Score.h>
#endif

#include <memory>

namespace benkyou {
   namespace model {

      /**
       * This class captures the result of a single test question, or a set of questions.
       */
      class TestScore
      {
            TestScore(const TestScore&) = delete;
            TestScore& operator=(const TestScore&) = delete;

            /** Default constructor */
         protected:
            TestScore();

            /** Destructor */
         public:
            virtual ~TestScore() = 0;

            /**
             * Get the overall test score.
             * @return the overall test score.
             */
         public:
            virtual Score score() const = 0;
      };
   }
}

#endif
