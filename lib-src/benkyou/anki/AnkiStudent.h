#ifndef _BENKYOU_ANKI_ANKISTUDENT_H
#define _BENKYOU_ANKI_ANKISTUDENT_H

#ifndef _BENKYOU_MODEL_STUDENT_H
#include <benkyou/model/Student.h>
#endif

namespace benkyou {
   namespace anki {
      class AnkiId;

      /**
       *  An anki student is just the user.
       */
      class AnkiStudent : public benkyou::model::Student
      {
            AnkiStudent() = delete;
            AnkiStudent(const AnkiStudent&) = delete;
            AnkiStudent& operator=(const AnkiStudent&) = delete;

            /**
             * constructor
             * @param id the anki id
             */
         public:
            AnkiStudent(::std::shared_ptr<const AnkiId> id);

            /** Destructor */
         public:
            ~AnkiStudent();

            ::std::shared_ptr< const benkyou::model::Id> id() const;
            ::std::vector< ::std::shared_ptr< const benkyou::model::Plan>> plans() const;
            void collectProgress(
                  benkyou::model::Collector< ::std::shared_ptr< const benkyou::model::Progress>>& collector) const;
            void saveProgress(::std::vector< ::std::shared_ptr< const benkyou::model::Progress>>& progress);

            /** The unique id */
         private:
            ::std::shared_ptr<const AnkiId> _id;
      };
   }
}

#endif
