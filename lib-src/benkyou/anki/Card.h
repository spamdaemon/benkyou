#ifndef _BENKYOU_ANKI_CARD_H
#define _BENKYOU_ANKI_CARD_H

#ifndef _BENKYOU_MODEL_ASPECT_H
#include <benkyou/model/Aspect.h>
#endif

#include <memory>
#include <vector>

namespace benkyou {
   namespace anki {
      class Note;
      class AnkiId;

      /**
       *  A card represents an aspect of an anki note. It does not provides access to
       *  templates used when presenting the card.
       */
      class Card : public benkyou::model::Aspect
      {
            Card(const Card&) = delete;
            Card& operator=(const Card&) = delete;
            Card() = delete;

            /**
             * Create a new card for the specified note.
             * @param id the anki id
             * @param note a note
             * @param name a name for this card.
             **/
         public:
            Card(::std::shared_ptr<const AnkiId> id, ::std::shared_ptr< const Note> note, ::std::string name);

            /** Destructor */
         public:
            ~Card();

          public:
            ::std::shared_ptr< const benkyou::model::Id> id() const;
           ::std::vector< ::std::shared_ptr< const benkyou::model::Target>> targets() const;
            ::std::shared_ptr< const benkyou::model::Concept> concept() const;

            /** The unique id */
          private:
             ::std::shared_ptr<const AnkiId> _id;

            /** The note for this card. */
         private:
            ::std::shared_ptr< const Note> _note;

             /** The card name */
         private:
            ::std::string _name;
      };
   }
}

#endif
