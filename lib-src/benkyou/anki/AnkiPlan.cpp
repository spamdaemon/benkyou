#include <benkyou/anki/AnkiPlan.h>
#include <benkyou/anki/AnkiStudent.h>
#include <benkyou/anki/Deck.h>
#include <benkyou/model/api.h>

#include <memory>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      AnkiPlan::AnkiPlan(::std::shared_ptr< const Deck> deck, ::std::shared_ptr< const AnkiStudent> student)
            : _deck(::std::move(deck)), _student(::std::move(student)), _totalCards(0), _unseenCards(0)
      {
      }

      AnkiPlan::~AnkiPlan()
      {

      }

      ::std::shared_ptr< const Goal> AnkiPlan::goal() const
      {
         return _deck;
      }

      ::std::shared_ptr< const Student> AnkiPlan::student() const
      {
         return _student;
      }

      ::std::unique_ptr< const Activity> AnkiPlan::nextActivity()
      {
         return nullptr;
      }

      size_t AnkiPlan::lessonCount() const
      {
         return _totalCards;
      }
      size_t AnkiPlan::lessonRemaining() const
      {
         return _unseenCards;
      }
      ::std::shared_ptr< Progress> AnkiPlan::progress() const
      {
         return nullptr;
      }

   }
}

