#include <benkyou/anki/AnkiId.h>
#include <benkyou/anki/Note.h>
#include <benkyou/anki/Card.h>
#include <algorithm>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      Note::Note(::std::shared_ptr< const AnkiId> id, ::std::map< ::std::string, ::std::string> data)
            : _id(::std::move(id)), _data(::std::move(data))
      {
      }

      Note::~Note()
      {
      }
      ::std::shared_ptr< const benkyou::model::Id> Note::id() const
      {
         return _id;
      }

      ::std::vector< ::std::shared_ptr< const benkyou::model::Target>> Note::targets() const
      {
         ::std::vector< ::std::shared_ptr< const benkyou::model::Target>> res(_cards.size());
         for (auto card : _cards) {
            res.push_back(card);
         }
         return res;
      }

      ::std::vector< ::std::shared_ptr< const benkyou::model::Aspect>> Note::aspects() const
      {
         ::std::vector< ::std::shared_ptr< const benkyou::model::Aspect>> res(_cards.size());
         for (auto card : _cards) {
            res.push_back(card);
         }
         return res;
      }

   }
}

