#include <benkyou/anki/AnkiId.h>
#include <benkyou/anki/AnkiStudent.h>
#include <benkyou/model/api.h>

#include <memory>
#include <vector>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      AnkiStudent::AnkiStudent(::std::shared_ptr< const AnkiId> id)
            : _id(::std::move(id))
      {

      }

      AnkiStudent::~AnkiStudent()
      {

      }
      ::std::shared_ptr< const benkyou::model::Id> AnkiStudent::id() const
      {
         return _id;
      }

      ::std::vector< ::std::shared_ptr< const Plan>> AnkiStudent::plans() const
      {
         return ::std::vector< ::std::shared_ptr< const Plan>>();
      }

      void AnkiStudent::collectProgress(
            benkyou::model::Collector< ::std::shared_ptr< const benkyou::model::Progress>>& collector) const
      {

      }
      void AnkiStudent::saveProgress(::std::vector< ::std::shared_ptr< const benkyou::model::Progress>>& progress)
      {

      }
   }
}

