#ifndef _BENKYOU_ANKI_ANKIPLAN_H
#define _BENKYOU_ANKI_ANKIPLAN_H

#include <stddef.h>
#include <memory>

#ifndef _BENKYOU_MODEL_PLAN_H
#include <benkyou/model/Plan.h>
#endif

namespace benkyou {
   namespace anki {
      class AnkiDeck;
      class AnkiStudent;
      class Deck;

      /**
       * A plan associates a student with a deck. This means a deck can be shared with different students.
       */
      class AnkiPlan : public benkyou::model::Plan
      {
            AnkiPlan() = delete;
            AnkiPlan(const AnkiPlan&) = delete;
            AnkiPlan& operator=(const AnkiPlan&) = delete;

            /** Default constructor */
         public:
            AnkiPlan(::std::shared_ptr< const Deck> deck, ::std::shared_ptr< const AnkiStudent> student);

            /** Destructor */
         public:
            ~AnkiPlan();

            ::std::shared_ptr< const benkyou::model::Goal> goal() const;
            ::std::shared_ptr< const benkyou::model::Student> student() const;
            ::std::unique_ptr< const benkyou::model::Activity> nextActivity();
            size_t lessonCount() const;
            size_t lessonRemaining() const;
            ::std::shared_ptr< benkyou::model::Progress> progress() const;

            /** The goal for this deck is just a tag */
         private:
            ::std::shared_ptr< const Deck> _deck;

            /** The student */
         private:
            ::std::shared_ptr< const AnkiStudent> _student;

            /** The total number of cards in the deck */
         private:
            size_t _totalCards;

            /** The number of unseen cards in the deck */
         private:
            size_t _unseenCards;

      };
   }
}

#endif
