#ifndef _BENKYOU_ANKI_FLASHCARD_H
#define _BENKYOU_ANKI_FLASHCARD_H

#ifndef _BENKYOU_MODEL_ACTIVITY_H
#include <benkyou/model/Activity.h>
#endif

#include <memory>
#include <vector>
#include <string>

namespace benkyou {
   namespace anki {
      class Deck;
      class Card;
      class AnkiPlan;

      /**
       *  This class implemented the activity of showing a card to the user. A flashcard
       *  consists of a card, and a front and back-template.
       */
      class FlashCard : public benkyou::model::Activity
      {
            FlashCard() = delete;
            FlashCard(const FlashCard&) = delete;
            FlashCard& operator=(const FlashCard&) = delete;

            /**
             * Create a new flash card
             * @param plan the plan
             * @param card the card that provides the data fort this activity
             * @param frontTemplate the template for the front of the card
             * @param backTemplate the template for the back of the card
             */
         public:
            FlashCard(::std::shared_ptr< const AnkiPlan> plan, ::std::shared_ptr< const Card> card,
                  ::std::string frontTemplate, ::std::string backTemplate);

            /** Destructor */
         public:
            ~FlashCard();

            /**
             * Get the text in the front of the card. If an empty string is returned, then
             * the card cannot be used.
             * @return the template for the front of this card.
             */
         public:
            ::std::string front() const;

            /**
             * Get the text in the back of this card. If an empty string is returned, then
             * the card cannot be used.
             * @return the template for the back of this card
             */
         public:
            ::std::string back() const;

         public:
            ::std::shared_ptr< const benkyou::model::Plan> plan() const;
            ::std::vector< ::std::shared_ptr< const benkyou::model::Aspect>> aspects() const;
            ::std::vector< ::std::shared_ptr< benkyou::model::Progress>> perform() const;

            /** The plan */
         private:
            ::std::shared_ptr< const AnkiPlan> _plan;

            /** The card that provides the data for this flashcard. */
         private:
            ::std::shared_ptr< const Card> _card;

            /** The front template */
         private:
            ::std::string _front;

            /** The back template */
         private:
            ::std::string _back;

      };
   }
}

#endif
