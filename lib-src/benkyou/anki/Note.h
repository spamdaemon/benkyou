#ifndef _BENKYOU_ANKI_NOTE_H
#define _BENKYOU_ANKI_NOTE_H

#include <memory>
#include <vector>

#ifndef _BENKYOU_MODEL_CONCEPT_H
#include <benkyou/model/Concept.h>
#endif

#include <map>
#include <string>

namespace benkyou {
   namespace anki {
      class Card;
      class AnkiId;

      /**
       *  A note provides the raw data that is presented on the flashcards.
       */
      class Note : public benkyou::model::Concept
      {
            Note(const Note&) = delete;
            Note& operator=(const Note&) = delete;
            Note() = delete;

            /**
             * Create a new note from a map of data.
             * @param id the anki id
             * @param data the date for this note
             */
         public:
            Note(::std::shared_ptr<const AnkiId> id, ::std::map< ::std::string, ::std::string> data);

            /** Destructor */
         public:
            ~Note();

         public:
            ::std::shared_ptr< const benkyou::model::Id> id() const;
            ::std::vector< ::std::shared_ptr< const benkyou::model::Target>> targets() const;
            ::std::vector< ::std::shared_ptr< const benkyou::model::Aspect>> aspects() const;

            /** The unique id */
          private:
             ::std::shared_ptr<const AnkiId> _id;

             /** The fields */
         private:
            ::std::map< ::std::string, ::std::string> _data;

            /** The cards */
         private:
            ::std::vector< ::std::shared_ptr< const Card>> _cards;
      };
   }
}

#endif
