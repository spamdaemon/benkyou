#include <benkyou/anki/AnkiPlan.h>
#include <benkyou/anki/Card.h>
#include <benkyou/anki/FlashCard.h>
#include <algorithm>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      FlashCard::FlashCard(::std::shared_ptr< const AnkiPlan> plan, ::std::shared_ptr< const Card> card,
            ::std::string frontTemplate, ::std::string backTemplate)
            : _plan(::std::move(plan)), _card(::std::move(card)), _front(::std::move(frontTemplate)), _back(
                  ::std::move(backTemplate))
      {
      }

      FlashCard::~FlashCard()
      {
      }

      ::std::string FlashCard::front() const
      {
         return _front;
      }
      ::std::string FlashCard::back() const
      {
         return _back;
      }

      ::std::shared_ptr< const benkyou::model::Plan> FlashCard::plan() const
      {
         return _plan;
      }

      ::std::vector< ::std::shared_ptr< const benkyou::model::Aspect>> FlashCard::aspects() const
      {
         ::std::vector< ::std::shared_ptr< const benkyou::model::Aspect>> res;
         res.push_back(_card);
         return res;
      }

      ::std::vector< ::std::shared_ptr< benkyou::model::Progress>> FlashCard::perform() const
      {
         ::std::vector< ::std::shared_ptr< benkyou::model::Progress>> res;

         return res;
      }

   }
}

