#ifndef _BENKYOU_ANKI_ANKIID_H
#define _BENKYOU_ANKI_ANKIID_H

#ifndef _BENKYOU_MODEL_ID_H
#include <benkyou/model/Id.h>
#endif

#include <string>

namespace benkyou {
   namespace anki {

      /**
       * A plan associated a student with an anki deck.
       */
      class AnkiId : public benkyou::model::Id
      {
            AnkiId() = delete;
            AnkiId(const AnkiId&) = delete;
            AnkiId& operator=(const AnkiId&) = delete;

            /**
             * Create an anki id.
             * @param id the id
             */
         public:
            AnkiId(::std::string id);

            /** Destructor */
         public:
            ~AnkiId();

         public:
            int compare(const Id& other) const;

            /** The id */
         private:
            const ::std::string _id;
      };
   }
}

#endif
