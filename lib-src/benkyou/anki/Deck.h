#ifndef _BENKYOU_ANKI_DECK_H
#define _BENKYOU_ANKI_DECK_H

#ifndef _BENKYOU_MODEL_GOAL_H
#include <benkyou/model/Goal.h>
#endif

#include <vector>
#include <memory>

namespace benkyou {
   namespace anki {
      class Card;
      class AnkiId;

      /**
       * A deck corresponds to a shared anki deck with the goal of mastering this deck.
       */
      class Deck : public benkyou::model::Goal, private ::std::enable_shared_from_this<Deck>
      {
            Deck(const Deck&) = delete;
            Deck& operator=(const Deck&) = delete;
            Deck() = delete;

            /**
             * Create a deck with cards.
             * @param id the anki id
             * @param cards the cards in this deck
             */
         public:
            Deck(::std::shared_ptr<const AnkiId> id,::std::vector< ::std::shared_ptr< const Card>> cards);

            /**
             * Load a deck from disk.
             * @param path a deck file
             * @return a pointer to a deck
             */
         public:
            static ::std::shared_ptr<Deck> load(const ::std::string& path);

            /** Destructor */
         public:
            ~Deck();

         public:
            ::std::shared_ptr< const benkyou::model::Id> id() const;
          ::std::shared_ptr< benkyou::model::Plan> getLessonPlan(
                  ::std::shared_ptr< const benkyou::model::Student> student);
            ::std::vector< ::std::shared_ptr< const benkyou::model::Target>> targets() const;

            /** The unique id */
          private:
             ::std::shared_ptr<const AnkiId> _id;

             /** The the cards in this deck */
         private:
            ::std::vector< ::std::shared_ptr< const Card>> _cards;
      };
   }
}

#endif
