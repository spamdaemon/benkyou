#include <benkyou/anki/AnkiId.h>

#include <algorithm>
#include <string>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      AnkiId::AnkiId(::std::string id)
            : _id(::std::move(id))
      {
      }

      AnkiId::~AnkiId()
      {

      }

      int AnkiId::compare(const Id& other) const
      {
         const AnkiId* oid = dynamic_cast< const AnkiId*>(&other);
         if (oid == nullptr) {
            return -1;
         }
         return _id.compare(oid->_id);
      }

   }
}

