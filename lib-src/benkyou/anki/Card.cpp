#include <benkyou/anki/Card.h>
#include <benkyou/anki/Note.h>
#include <benkyou/anki/AnkiId.h>

#include <algorithm>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      Card::Card(::std::shared_ptr< const AnkiId> id, ::std::shared_ptr< const Note> note, ::std::string name)
            : _id(::std::move(id)), _note(::std::move(note)), _name(::std::move(name))
      {
      }

      Card::~Card()
      {
      }
      ::std::shared_ptr< const benkyou::model::Id> Card::id() const
      {
         return _id;
      }

      ::std::shared_ptr< const Concept> Card::concept() const
      {
         return _note;
      }

      ::std::vector< ::std::shared_ptr< const benkyou::model::Target>> Card::targets() const
      {
         return ::std::vector< ::std::shared_ptr< const benkyou::model::Target>>();
      }

   }
}

