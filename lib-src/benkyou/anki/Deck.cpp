#include <benkyou/anki/AnkiId.h>
#include <benkyou/anki/AnkiPlan.h>
#include <benkyou/anki/AnkiStudent.h>
#include <benkyou/anki/Deck.h>
#include <benkyou/anki/Card.h>
#include <algorithm>
#include <string>

namespace benkyou {
   namespace anki {
      using namespace benkyou::model;

      Deck::Deck(::std::shared_ptr< const AnkiId> id, ::std::vector< ::std::shared_ptr< const Card>> cards)
            : _id(::std::move(id)), _cards(::std::move(cards))
      {

      }

      ::std::shared_ptr< Deck> Deck::load(const ::std::string& path)
      {
         return nullptr;
      }

      Deck::~Deck()
      {

      }
      ::std::shared_ptr< const benkyou::model::Id> Deck::id() const
      {
         return _id;
      }

      ::std::shared_ptr< Plan> Deck::getLessonPlan(::std::shared_ptr< const Student> student)
      {
         auto s = ::std::dynamic_pointer_cast< const AnkiStudent>(student);
         ::std::shared_ptr< Plan> res(new AnkiPlan(shared_from_this(), s));
         return res;
      }

      ::std::vector< ::std::shared_ptr< const Target>> Deck::targets() const
      {
         ::std::vector< ::std::shared_ptr< const Target>> res(_cards.size());
         for (auto card : _cards) {
            res.push_back(card);
         }
         return res;
      }

   }
}

